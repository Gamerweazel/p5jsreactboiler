REQUIREMENTS
=============
1. nodeJS && npm

SETUP
=============
1. Install webpack and nodemon globally for best dev experience.

    npm i -g webpack nodemon

2. Install dependencies (make sure you're in the project's root folder)

    npm i

3. Open two terminals in project folder

4. In first terminal run command "webpack" to generate bundle and watch for changes

5. In second terminal type "npm start" to begin server

6. In a web browser navigate to localhost:8000, you should see a p5js sketch running as a stateful react component

EDITING SKETCHES
=============
1. To edit a sketch, locate the projectroot/src/sketches folder and choose a file to edit
  refer to p5js documentation and reference for examples and tutorials
  *make sure all p5js functionality is prefaced with a 'p.' due to the instantiation required by webpack