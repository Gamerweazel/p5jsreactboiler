const path = require('path');

module.exports = {
    entry: [
        './src/Main'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    watch: true,
    debug: true,
    devtool: 'source-map',
    module: {
        loaders: [{
            loader: 'babel-loader',
            include: [
                path.resolve(__dirname, 'src')
            ],
            test: /\.jsx?$/,
            query: {
                presets: ['es2015', 'react']
            }
        }]
    }
};
