const express = require('express');
const path = require('path');
const app = express();

const static_path = path.join(__dirname, 'dist');

app.use(express.static(static_path))
    .get('/*', function(req, res) {
        res.sendFile('index.html', {
            root: static_path
        });
    }).listen(process.env.PORT || 8000, function(err) {
        if (err) {
            console.log(err)
        }
        console.log('Listening at port 8000');
    });
