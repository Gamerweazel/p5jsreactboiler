import { Route, IndexRoute } from 'react-router'
import React from 'react'
import App from './App'
import HomePage from './components/HomePage.js'

export const routes = (
    <Route path='/' component={App}>
      <IndexRoute component={HomePage}/>
    </Route>
)
