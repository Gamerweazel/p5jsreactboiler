import React, { Component } from 'react'
import P5Component from './P5Component'
import { example } from '../sketches/example'
import { example2 } from '../sketches/example2'

export default class HomePage extends Component {
    constructor(props) {
        super(props)
        this.changeSketch = this.changeSketch.bind(this)
        this.state = {
            selectedSketch: example2
        }
    }

    changeSketch() {
        let sketch = this.state.selectedSketch == example2 ? example : example2;
        this.setState({selectedSketch: sketch});
    }

    render() {
        return(
            <div>
                <P5Component sketch={this.state.selectedSketch} />
                <button onClick={this.changeSketch}>Change Sketch</button>
            </div>
        )
    }
}
