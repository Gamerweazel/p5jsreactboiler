export const example = (p) => {
    let val = 20;
    p.setup = () => {
        p.createCanvas(300, 300);
        p.noStroke();
    }
    p.draw = () => {
        p.background(val);
        p.rect(p.width/2 - 25, p.height/2 - 25, 50, 50);
    }
    p.mousePressed = () => {
        val = (val + 16) % 256;
    }
}
