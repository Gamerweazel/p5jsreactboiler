export const example2 = (p) => {
    p.setup = () => {
        p.createCanvas(300, 300);
        p.noStroke();
        p.background(80);
    }
    p.draw = () => {
        if (p.mouseIsPressed) {
            p.fill(280);
        } else {
            p.fill(0, 255, 0);
        }
        p.ellipse(p.mouseX, p.mouseY, 20, 20);
    }
}
